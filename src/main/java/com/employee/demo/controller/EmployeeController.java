package com.employee.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.employee.demo.entity.Employee;
import com.employee.demo.service.EmployeeServiceImplementations;

@Controller
@RequestMapping("employees")
public class EmployeeController {
	
	private EmployeeServiceImplementations serimp;

	@Autowired
	public EmployeeController(EmployeeServiceImplementations serimp) {
		this.serimp = serimp;
	}
	
	@GetMapping("/list")
	public String fetchAll(Model model) {
		List<Employee> employee= serimp.displayAll();
		model.addAttribute("EMPLOYEES",employee);
		return "employee/list-employees";
	}
	
	@GetMapping("/showFormForAdd")
	public String getForm(Model model) {
		Employee emp=new Employee();
		model.addAttribute("EMPLOYEE",emp);
		return "employee/employee-form";
	}
	
	@PostMapping("/save")
	public String addEmployee(@ModelAttribute("EMPLOYEE") Employee emp) {
		serimp.insertOrUpdate(emp);
		return "redirect:/employees/list";
		
	}
	@GetMapping("/showFormForUpdate")
	public String showForm(@RequestParam("employeeId") int empId,Model model) {
		Employee emp=serimp.getById(empId); // employee existed or not
		model.addAttribute("EMPLOYEE",emp);
		return "employee/employee-form"; // old data transfer
	}
	
	@GetMapping("/delete")
	public String deleteEmployee(@RequestParam("employeeId") int empId) {
		serimp.removeById(empId);
		return "redirect:/employees/list";
	}
	
}
