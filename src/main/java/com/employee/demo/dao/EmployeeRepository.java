package com.employee.demo.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.demo.entity.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
//	List<Employee> findByName(String firstName);
}
