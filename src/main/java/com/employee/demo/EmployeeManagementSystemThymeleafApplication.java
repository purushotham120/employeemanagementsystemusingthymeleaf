package com.employee.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeManagementSystemThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagementSystemThymeleafApplication.class, args);
	}

}
