package com.employee.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.demo.dao.EmployeeRepository;
import com.employee.demo.entity.Employee;

@Service
public class EmployeeServiceImplementations {
	
	private EmployeeRepository emprep;
	
	@Autowired
	public EmployeeServiceImplementations(EmployeeRepository emprep) {
		this.emprep=emprep;
	}
	
	@Transactional
	public List<Employee> displayAll(){
		List<Employee> emp=emprep.findAll();
		return emp;
	}
	
	@Transactional
	public Employee getById(int id) {
		return emprep.findById(id).get();
	}
	
	@Transactional
	public void insertOrUpdate(Employee emp) {
		emprep.save(emp);
	}
	
	@Transactional
	public void removeById(int id) {
		emprep.deleteById(id);
	}
	

}
